﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Template
{
    public partial class MainForm : Form
    {
        BinaryTree<int> intTree;
        BinaryTreeVisualiser<int> intTreeVisualiser;

        BinaryTree<string> strTree;
        BinaryTreeVisualiser<string> strTreeVisualiser;

        bool inIntTree = true;

        public MainForm()
        {
            InitializeComponent();
            
            intTree = new BinaryTree<int>();
            intTree.AddToHead(1);

            intTree.Head.Right = new BinaryTreeNode<int>(3);
            intTree.Head.Left = new BinaryTreeNode<int>(2);

            intTree.Head.Left.Left = new BinaryTreeNode<int>(10);
            intTree.Head.Left.Right = new BinaryTreeNode<int>(11);

            intTree.Head.Right.Left = new BinaryTreeNode<int>(4);
            intTree.Head.Right.Left.Right = new BinaryTreeNode<int>(5);
            intTree.Head.Right.Left.Right.Left = new BinaryTreeNode<int>(6);

            strTree = new BinaryTree<string>();
            strTree.AddToHead("aaa");

            strTree.Head.Right = new BinaryTreeNode<string>("abb");
            strTree.Head.Left = new BinaryTreeNode<string>("text");
            strTree.Head.Left.Left = new BinaryTreeNode<string>("text1");
            strTree.Head.Left.Left.Right = new BinaryTreeNode<string>("text2");

            intTreeVisualiser = new BinaryTreeVisualiser<int>(intTree);
            visualizerContainer.Controls.Add(intTreeVisualiser);
            intTreeVisualiser.Dock = DockStyle.Fill;

            strTreeVisualiser = new BinaryTreeVisualiser<string>(strTree);
            strTreeVisualiser.Dock = DockStyle.Fill;
        }

        private void delSubTreeBtn_Click(object sender, EventArgs e)
        {
            if (inIntTree)
            {
                try
                {
                    intTree.DeleteSubTree(intTree.Head, int.Parse(textBox.Text));
                    intTreeVisualiser.Invalidate();
                }
                catch (FormatException)
                {
                    MessageBox.Show("Число в неправильном формате");
                }
            }
            else
            {
                strTree.DeleteSubTree(strTree.Head, textBox.Text);
                strTreeVisualiser.Invalidate();
            }
        }

        private void changeTreeBtn_Click(object sender, EventArgs e)
        {
            visualizerContainer.Controls.Clear();
            if (inIntTree)
            {
                visualizerContainer.Controls.Add(strTreeVisualiser);
                changeTreeBtn.Text = "Переключиться на числа";
                inIntTree = false;
            }
            else
            {
                visualizerContainer.Controls.Add(intTreeVisualiser);
                changeTreeBtn.Text = "Переключиться на строки";
                inIntTree = true;
            }
        }
    }
}
