﻿using System;

namespace Template
{
    class BinaryTreeNode<T> where T : IComparable
    {
        public BinaryTreeNode<T> Left { get; set; }
        public BinaryTreeNode<T> Right { get; set; }
        public T Value { get; set; }

        public BinaryTreeNode(T value)
        {
            Value = value;
        }
    }
}
