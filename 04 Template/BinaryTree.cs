﻿using System;

namespace Template
{
    class BinaryTree<T> where T : IComparable
    {
        public BinaryTreeNode<T> Head { get; private set; }

        private int recurseGetDepth(BinaryTreeNode<T> node, int depth)
        {
            if (node is null)
            {
                return depth--;
            }

            int leftDepth = recurseGetDepth(node.Left, depth + 1);
            int rightDepth = recurseGetDepth(node.Right, depth + 1);

            return Math.Max(leftDepth, rightDepth);
        }

        public int Depth
        {
            get
            {
                return recurseGetDepth(Head, 0);
            }
        }

        public void AddToHead(T value)
        {
            Random r = new Random();
            BinaryTreeNode<T> newNode = new BinaryTreeNode<T>(value);

            if (Head is null)
            {
                Head = newNode;
                return;
            }

            BinaryTreeNode<T> tmp = Head;
            Head = newNode;
            if (r.Next() % 2 == 0)
            {
                newNode.Left = tmp;
            }
            else
            {
                newNode.Right = tmp;
            }
        }

        public void DeleteSubTree(BinaryTreeNode<T> node, T maxValue)
        {
            if (node == null)
            {
                return;
            }

            if (node.Value.CompareTo(maxValue) > 0)
            {
                node.Left = null;
                node.Right = null;
                return;
            }

            DeleteSubTree(node.Left, maxValue);
            DeleteSubTree(node.Right, maxValue);
        }
    }
}
