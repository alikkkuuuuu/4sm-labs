﻿namespace Template
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.visualizerContainer = new System.Windows.Forms.Panel();
            this.controlsPanel = new System.Windows.Forms.Panel();
            this.subTreeLabel = new System.Windows.Forms.Label();
            this.delSubTreeBtn = new System.Windows.Forms.Button();
            this.textBox = new System.Windows.Forms.TextBox();
            this.changeTreeBtn = new System.Windows.Forms.Button();
            this.controlsPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // visualizerContainer
            // 
            this.visualizerContainer.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.visualizerContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.visualizerContainer.Location = new System.Drawing.Point(0, 0);
            this.visualizerContainer.Name = "visualizerContainer";
            this.visualizerContainer.Size = new System.Drawing.Size(784, 528);
            this.visualizerContainer.TabIndex = 0;
            // 
            // controlsPanel
            // 
            this.controlsPanel.Controls.Add(this.changeTreeBtn);
            this.controlsPanel.Controls.Add(this.textBox);
            this.controlsPanel.Controls.Add(this.subTreeLabel);
            this.controlsPanel.Controls.Add(this.delSubTreeBtn);
            this.controlsPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.controlsPanel.Location = new System.Drawing.Point(0, 528);
            this.controlsPanel.Name = "controlsPanel";
            this.controlsPanel.Size = new System.Drawing.Size(784, 33);
            this.controlsPanel.TabIndex = 0;
            // 
            // subTreeLabel
            // 
            this.subTreeLabel.AutoSize = true;
            this.subTreeLabel.Location = new System.Drawing.Point(562, 8);
            this.subTreeLabel.Name = "subTreeLabel";
            this.subTreeLabel.Size = new System.Drawing.Size(38, 13);
            this.subTreeLabel.TabIndex = 2;
            this.subTreeLabel.Text = "Vi max";
            // 
            // delSubTreeBtn
            // 
            this.delSubTreeBtn.Location = new System.Drawing.Point(12, 6);
            this.delSubTreeBtn.Name = "delSubTreeBtn";
            this.delSubTreeBtn.Size = new System.Drawing.Size(223, 23);
            this.delSubTreeBtn.TabIndex = 0;
            this.delSubTreeBtn.Text = "Удалить поддерево";
            this.delSubTreeBtn.UseVisualStyleBackColor = true;
            this.delSubTreeBtn.Click += new System.EventHandler(this.delSubTreeBtn_Click);
            // 
            // textBox
            // 
            this.textBox.Location = new System.Drawing.Point(607, 8);
            this.textBox.Name = "textBox";
            this.textBox.Size = new System.Drawing.Size(165, 20);
            this.textBox.TabIndex = 3;
            // 
            // changeTreeBtn
            // 
            this.changeTreeBtn.Location = new System.Drawing.Point(241, 7);
            this.changeTreeBtn.Name = "changeTreeBtn";
            this.changeTreeBtn.Size = new System.Drawing.Size(223, 23);
            this.changeTreeBtn.TabIndex = 4;
            this.changeTreeBtn.Text = "Переключиться на строки";
            this.changeTreeBtn.UseVisualStyleBackColor = true;
            this.changeTreeBtn.Click += new System.EventHandler(this.changeTreeBtn_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.visualizerContainer);
            this.Controls.Add(this.controlsPanel);
            this.Name = "MainForm";
            this.ShowIcon = false;
            this.Text = "Бинарное дерево";
            this.controlsPanel.ResumeLayout(false);
            this.controlsPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel visualizerContainer;
        private System.Windows.Forms.Panel controlsPanel;
        private System.Windows.Forms.Label subTreeLabel;
        private System.Windows.Forms.Button delSubTreeBtn;
        private System.Windows.Forms.TextBox textBox;
        private System.Windows.Forms.Button changeTreeBtn;
    }
}

