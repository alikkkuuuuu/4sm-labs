﻿using System;
using System.Collections;
using System.Drawing;
using System.Windows.Forms;
using System.Xml.Linq;

namespace Template
{
    class BinaryTreeVisualiser<T> : Control 
        where T : IComparable
    {
        BinaryTree<T> tree;
        Pen pen = new Pen(Color.Black);

        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);
            Invalidate();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            if (tree != null && tree.Head != null)
            {
                drawTree(e);
            }
            else
            {
                e.Graphics.DrawString(
                    "Дерево пустое",
                    new Font("Microsoft Sans Serif", 11.25F, FontStyle.Regular, GraphicsUnit.Point, 204),
                    new SolidBrush(ForeColor),
                    ClientRectangle,
                    new StringFormat());
            }
        }

        private void drawTree(PaintEventArgs e)
        {
            Point horizCenter = new Point(Width / 2, 25);
            int vertDistribution = Height / tree.Depth;
            recurseDraw(tree.Head, horizCenter, vertDistribution, 0, e);
        }

        private void recurseDraw(BinaryTreeNode<T> node, Point center, int vertDistribution, int curDepth, PaintEventArgs e)
        {
            if (node is null)
            {
                return;
            }

            int maxNodesAtDepth = (int)Math.Pow(2, curDepth);
            int horizDistrib = Width / maxNodesAtDepth;

            if (node.Left != null)
            {
                Point leftNode = new Point(center.X - horizDistrib / 4, center.Y + vertDistribution);
                e.Graphics.DrawLine(new Pen(ForeColor), center, leftNode);
                recurseDraw(node.Left, leftNode, vertDistribution, ++curDepth, e);
            }

            if (node.Right != null)
            {
                Point rightNode = new Point(center.X + horizDistrib / 4, center.Y + vertDistribution);
                e.Graphics.DrawLine(new Pen(ForeColor), center, rightNode);
                recurseDraw(node.Right, rightNode, vertDistribution, ++curDepth, e);
            }

            drawEllipseWithText(node.Value.ToString(), center, e);
        }

        private void drawEllipseWithText(string text, Point center, PaintEventArgs e)
        {
            Rectangle rec = new Rectangle(center, new Size(25, 25));
            rec.X -= rec.Width / 2;
            rec.Y -= rec.Height / 2;
            e.Graphics.FillEllipse(new SolidBrush(BackColor), rec);
            e.Graphics.DrawEllipse(pen, rec);
            using (StringFormat sf = new StringFormat())
            {
                sf.Alignment = StringAlignment.Center;
                sf.LineAlignment = StringAlignment.Center;
                e.Graphics.DrawString(
                    text,
                    new Font("Microsoft Sans Serif", 10F, FontStyle.Regular, GraphicsUnit.Pixel, 204),
                    new SolidBrush(ForeColor),
                    rec,
                    sf);
            }
        }

        public BinaryTreeVisualiser(BinaryTree<T> tree)
        {
            this.tree = tree;
        }
    }
}
