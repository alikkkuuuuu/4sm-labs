﻿using System.Windows;
using System.Windows.Documents;
using System.Text.RegularExpressions;

namespace RegExp
{
    public partial class MainWindow : Window
    {
        private void findDateInTextBox()
        {
            string pattern = @"\d{4}/\d{2}/\d{2}\s\d{2}:\d{2}(:\d{2})?";
            string text = mainTextBlock.Text;
            Match match = Regex.Match(text, pattern);
            foreach (Capture c in match.Captures)
            {
                mainTextBlock.Text = mainTextBlock.Text.Remove(c.Index);
                mainTextBlock.Inlines.Add(new Italic(new Run(c.Value)));
                mainTextBlock.Inlines.Add(new Run(text.Substring(c.Index + c.Length)));
            }
        }

        public MainWindow()
        {
            InitializeComponent();
            findDateInTextBox();
        }
    }
}
