﻿namespace Parallel
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.piWorker = new System.ComponentModel.BackgroundWorker();
            this.piProgressBar = new System.Windows.Forms.ProgressBar();
            this.startButton = new System.Windows.Forms.Button();
            this.stopButton = new System.Windows.Forms.Button();
            this.pauseButton = new System.Windows.Forms.Button();
            this.continueButton = new System.Windows.Forms.Button();
            this.precisionNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.vietResult = new System.Windows.Forms.Label();
            this.vietLabel = new System.Windows.Forms.Label();
            this.leibnizLabel = new System.Windows.Forms.Label();
            this.wallisLabel = new System.Windows.Forms.Label();
            this.leibnizResult = new System.Windows.Forms.Label();
            this.wallisResult = new System.Windows.Forms.Label();
            this.vietProgress = new System.Windows.Forms.Label();
            this.leibnizProgress = new System.Windows.Forms.Label();
            this.wallisProgress = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.precisionNumericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // piWorker
            // 
            this.piWorker.WorkerReportsProgress = true;
            this.piWorker.WorkerSupportsCancellation = true;
            this.piWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.piWorker_DoWork);
            this.piWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.piWorker_ProgressChanged);
            this.piWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.piWorker_RunWorkerCompleted);
            // 
            // piProgressBar
            // 
            this.piProgressBar.Location = new System.Drawing.Point(15, 134);
            this.piProgressBar.Name = "piProgressBar";
            this.piProgressBar.Size = new System.Drawing.Size(560, 23);
            this.piProgressBar.TabIndex = 0;
            // 
            // startButton
            // 
            this.startButton.Location = new System.Drawing.Point(15, 104);
            this.startButton.Name = "startButton";
            this.startButton.Size = new System.Drawing.Size(80, 23);
            this.startButton.TabIndex = 1;
            this.startButton.Text = "Старт";
            this.startButton.UseVisualStyleBackColor = true;
            this.startButton.Click += new System.EventHandler(this.startButton_Click);
            // 
            // stopButton
            // 
            this.stopButton.Enabled = false;
            this.stopButton.Location = new System.Drawing.Point(116, 104);
            this.stopButton.Name = "stopButton";
            this.stopButton.Size = new System.Drawing.Size(80, 23);
            this.stopButton.TabIndex = 2;
            this.stopButton.Text = "Остановить";
            this.stopButton.UseVisualStyleBackColor = true;
            this.stopButton.Click += new System.EventHandler(this.stopButton_Click);
            // 
            // pauseButton
            // 
            this.pauseButton.Enabled = false;
            this.pauseButton.Location = new System.Drawing.Point(217, 104);
            this.pauseButton.Name = "pauseButton";
            this.pauseButton.Size = new System.Drawing.Size(80, 23);
            this.pauseButton.TabIndex = 3;
            this.pauseButton.Text = "Пауза";
            this.pauseButton.UseVisualStyleBackColor = true;
            this.pauseButton.Click += new System.EventHandler(this.pauseButton_Click);
            // 
            // continueButton
            // 
            this.continueButton.Enabled = false;
            this.continueButton.Location = new System.Drawing.Point(318, 104);
            this.continueButton.Name = "continueButton";
            this.continueButton.Size = new System.Drawing.Size(80, 23);
            this.continueButton.TabIndex = 4;
            this.continueButton.Text = "Продолжить";
            this.continueButton.UseVisualStyleBackColor = true;
            this.continueButton.Click += new System.EventHandler(this.continueButton_Click);
            // 
            // precisionNumericUpDown
            // 
            this.precisionNumericUpDown.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.precisionNumericUpDown.Location = new System.Drawing.Point(419, 105);
            this.precisionNumericUpDown.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.precisionNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.precisionNumericUpDown.Name = "precisionNumericUpDown";
            this.precisionNumericUpDown.Size = new System.Drawing.Size(153, 20);
            this.precisionNumericUpDown.TabIndex = 5;
            this.precisionNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(419, 89);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(156, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Точность (знаков после зап.)";
            // 
            // vietResult
            // 
            this.vietResult.AutoSize = true;
            this.vietResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.vietResult.Location = new System.Drawing.Point(174, 9);
            this.vietResult.Name = "vietResult";
            this.vietResult.Size = new System.Drawing.Size(19, 20);
            this.vietResult.TabIndex = 7;
            this.vietResult.Text = "0";
            // 
            // vietLabel
            // 
            this.vietLabel.AutoSize = true;
            this.vietLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.vietLabel.Location = new System.Drawing.Point(9, 9);
            this.vietLabel.Name = "vietLabel";
            this.vietLabel.Size = new System.Drawing.Size(134, 20);
            this.vietLabel.TabIndex = 10;
            this.vietLabel.Text = "Формула Виета:";
            // 
            // leibnizLabel
            // 
            this.leibnizLabel.AutoSize = true;
            this.leibnizLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.leibnizLabel.Location = new System.Drawing.Point(9, 34);
            this.leibnizLabel.Name = "leibnizLabel";
            this.leibnizLabel.Size = new System.Drawing.Size(162, 20);
            this.leibnizLabel.TabIndex = 11;
            this.leibnizLabel.Text = "Формула Лейбница:";
            // 
            // wallisLabel
            // 
            this.wallisLabel.AutoSize = true;
            this.wallisLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.wallisLabel.Location = new System.Drawing.Point(9, 59);
            this.wallisLabel.Name = "wallisLabel";
            this.wallisLabel.Size = new System.Drawing.Size(153, 20);
            this.wallisLabel.TabIndex = 12;
            this.wallisLabel.Text = "Формула Валлиса:";
            // 
            // leibnizResult
            // 
            this.leibnizResult.AutoSize = true;
            this.leibnizResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.leibnizResult.Location = new System.Drawing.Point(174, 34);
            this.leibnizResult.Name = "leibnizResult";
            this.leibnizResult.Size = new System.Drawing.Size(19, 20);
            this.leibnizResult.TabIndex = 13;
            this.leibnizResult.Text = "0";
            // 
            // wallisResult
            // 
            this.wallisResult.AutoSize = true;
            this.wallisResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.wallisResult.Location = new System.Drawing.Point(174, 59);
            this.wallisResult.Name = "wallisResult";
            this.wallisResult.Size = new System.Drawing.Size(19, 20);
            this.wallisResult.TabIndex = 14;
            this.wallisResult.Text = "0";
            // 
            // vietProgress
            // 
            this.vietProgress.AutoSize = true;
            this.vietProgress.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.vietProgress.Location = new System.Drawing.Point(559, 9);
            this.vietProgress.Name = "vietProgress";
            this.vietProgress.Size = new System.Drawing.Size(13, 20);
            this.vietProgress.TabIndex = 15;
            this.vietProgress.Text = ".";
            // 
            // leibnizProgress
            // 
            this.leibnizProgress.AutoSize = true;
            this.leibnizProgress.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.leibnizProgress.Location = new System.Drawing.Point(559, 34);
            this.leibnizProgress.Name = "leibnizProgress";
            this.leibnizProgress.Size = new System.Drawing.Size(13, 20);
            this.leibnizProgress.TabIndex = 16;
            this.leibnizProgress.Text = ".";
            // 
            // wallisProgress
            // 
            this.wallisProgress.AutoSize = true;
            this.wallisProgress.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.wallisProgress.Location = new System.Drawing.Point(559, 59);
            this.wallisProgress.Name = "wallisProgress";
            this.wallisProgress.Size = new System.Drawing.Size(13, 20);
            this.wallisProgress.TabIndex = 17;
            this.wallisProgress.Text = ".";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 171);
            this.Controls.Add(this.wallisProgress);
            this.Controls.Add(this.leibnizProgress);
            this.Controls.Add(this.vietProgress);
            this.Controls.Add(this.wallisResult);
            this.Controls.Add(this.leibnizResult);
            this.Controls.Add(this.wallisLabel);
            this.Controls.Add(this.leibnizLabel);
            this.Controls.Add(this.vietLabel);
            this.Controls.Add(this.vietResult);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.precisionNumericUpDown);
            this.Controls.Add(this.continueButton);
            this.Controls.Add(this.pauseButton);
            this.Controls.Add(this.stopButton);
            this.Controls.Add(this.startButton);
            this.Controls.Add(this.piProgressBar);
            this.MaximumSize = new System.Drawing.Size(600, 210);
            this.MinimumSize = new System.Drawing.Size(600, 210);
            this.Name = "MainForm";
            this.ShowIcon = false;
            this.Text = "Вычисление числа Пи";
            ((System.ComponentModel.ISupportInitialize)(this.precisionNumericUpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.ComponentModel.BackgroundWorker piWorker;
        private System.Windows.Forms.ProgressBar piProgressBar;
        private System.Windows.Forms.Button startButton;
        private System.Windows.Forms.Button stopButton;
        private System.Windows.Forms.Button pauseButton;
        private System.Windows.Forms.Button continueButton;
        private System.Windows.Forms.NumericUpDown precisionNumericUpDown;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label vietResult;
        private System.Windows.Forms.Label vietLabel;
        private System.Windows.Forms.Label leibnizLabel;
        private System.Windows.Forms.Label wallisLabel;
        private System.Windows.Forms.Label leibnizResult;
        private System.Windows.Forms.Label wallisResult;
        private System.Windows.Forms.Label vietProgress;
        private System.Windows.Forms.Label leibnizProgress;
        private System.Windows.Forms.Label wallisProgress;
    }
}

