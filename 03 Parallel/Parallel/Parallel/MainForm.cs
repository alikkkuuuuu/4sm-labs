﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Diagnostics;

namespace Parallel
{
    public partial class MainForm : Form
    {
        ManualResetEvent mre = new ManualResetEvent(false);

        public MainForm()
        {
            InitializeComponent();
        }

        private void calculateViet(object data)
        {
            ToLabel(vietProgress, "-");
            if (data is double)
            {
                double e = (double)data;
                double purple = Math.Sqrt(2);
                double answer = purple / 2;
                double pi;

                do
                {
                    purple = Math.Sqrt(2 + purple);
                    answer = answer * (purple / 2);
                    pi = 2 / answer;

                    mre.WaitOne();
                } while (Math.PI - pi > e);
                ToLabel(vietResult, pi.ToString());
            }

            ToLabel(vietProgress, "+");
            piWorker.ReportProgress(33);
            mre.WaitOne();
        }

        private void calculateLeibniz(object data)
        {
            ToLabel(leibnizProgress, "-");
            if (data is double)
            {
                double e = (double)data;
                double answer = 0;
                double pi;

                double i = 0;
                do
                {
                    answer += Math.Pow(-1, i) / (2 * i + 1);
                    pi = answer * 4;
                    i++;

                    mre.WaitOne();
                } while (Math.Abs(pi - Math.PI) > e);
                ToLabel(leibnizResult, pi.ToString());
            }

            ToLabel(leibnizProgress, "+");
            piWorker.ReportProgress(33);
            mre.WaitOne();
        }

        private void calculateWallis(object data)
        {
            ToLabel(wallisProgress, "-");
            if (data is double)
            {
                double e = (double)data;
                double pi = 2;

                for (int i = 1; Math.Abs(pi - Math.PI) > e; i++)
                {
                    double left = (double)(2 * i) / (2 * i - 1);
                    double right = (double)(2 * i) / (2 * i + 1);
                    pi *= left * right;
                }
                ToLabel(wallisResult, pi.ToString());
            }

            ToLabel(wallisProgress, "+");
            piWorker.ReportProgress(33);
            mre.WaitOne();
        }

        private void ToLabel(Label label, string text)
        {
            if (InvokeRequired)
                Invoke((Action<Label, string>)ToLabel, label, text);
            else
                label.Text = text;
        }

        private void piWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            double prec = Math.Pow(10, -(double)precisionNumericUpDown.Value - 1);
            Thread[] threads = new Thread[3];
            threads[0] = new Thread(calculateViet);
            threads[1] = new Thread(calculateLeibniz);
            threads[2] = new Thread(calculateWallis);

            Thread pollThread = new Thread(() =>
            {
                while (true)
                {
                    Thread.Sleep(50);
                    if (piWorker.CancellationPending)
                    {
                        e.Cancel = true;
                        foreach (Thread t in threads)
                            t.Abort();
                        return;
                    }
                }
            });
            pollThread.Start();

            for (int i = 0; i < threads.Length; i++)
                threads[i].Start(prec);

            for (int i = 0; i < threads.Length; i++)
            {
                threads[i].Join();
            }

            pollThread.Abort();
        }

        private void piWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            startButton.Enabled = true;
            stopButton.Enabled = false;
            pauseButton.Enabled = false;
            continueButton.Enabled = false;

            vietProgress.Text = ".";
            leibnizProgress.Text = ".";
            wallisProgress.Text = ".";

            piProgressBar.Value = 0;
            if (!e.Cancelled)
                MessageBox.Show("Работа завершена");
            else
                MessageBox.Show("Работа была отменена");
        }

        private void piWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            piProgressBar.Value += e.ProgressPercentage;
        }

        private void startButton_Click(object sender, EventArgs e)
        {
            piWorker.RunWorkerAsync();
            startButton.Enabled = false;
            stopButton.Enabled = true;
            pauseButton.Enabled = true;

            mre.Set();
        }

        private void stopButton_Click(object sender, EventArgs e)
        {
            piWorker.CancelAsync();
            mre.Set();
        }

        private void pauseButton_Click(object sender, EventArgs e)
        {
            continueButton.Enabled = true;
            pauseButton.Enabled = false;

            mre.Reset();
        }

        private void continueButton_Click(object sender, EventArgs e)
        {
            continueButton.Enabled = false;
            pauseButton.Enabled = true;

            mre.Set();
        }
    }
}
