﻿using System;
using System.Collections.Generic;

namespace TreeWPF
{
    class Tree
    {
        readonly static Dictionary<TreeType, string> translation = new Dictionary<TreeType, string> 
        {
            { TreeType.Coniferous, "Хвойное"},
            { TreeType.Pine, "Сосновое"},
            { TreeType.Cypress, "Кипарисовое"},
            { TreeType.Fruit, "Плодовое"},
        };

        public DateTime PlantTime { get; set; }         // Дата посадки дерева
        public int Age { 
            get
            {
                return (DateTime.Now - PlantTime).Days / 365;
            }
        }                                               // Возраст
        public int Density { get; set; }                // Плотность (кг/м)
        public int HeatConductivity { get; set; }       // Теплопроводимость (Вт/(м * к))
        public int ElectricConductivity { get; set; }   // Электропроводимость (Сименс)
        public TreeType Type { get; set; }              // Тип дерева
        public string RussianType { 
            get
            {
                return translation[Type];
            }
        }

        private Tree()
        { }

        public Tree(int age, int density, int heatConductivity, 
            int electricConductivity, TreeType treeType)
            : this(DateTime.Now.AddDays(-(age * 365)), density, 
                  heatConductivity, electricConductivity, treeType)
        { }

        public Tree(DateTime plantTime, int density, int heatConductivity,
            int electricConductivity, TreeType treeType)
        {
            PlantTime = plantTime;
            Density = density;
            HeatConductivity = heatConductivity;
            ElectricConductivity = electricConductivity;
            Type = treeType;
        }

        public static bool TryParseRussianType(string rusType, out TreeType result)
        {
            foreach (var kv in translation)
            {
                if (rusType.ToLowerInvariant().CompareTo(kv.Value.ToLowerInvariant()) == 0)
                {
                    result = kv.Key;
                    return true;
                }
            }

            result = TreeType.Coniferous;
            return false;
        }

        public override string ToString()
        {
            return $"{Age} лет, {Density} кг/м, {HeatConductivity} Вт/(м * к), {ElectricConductivity} Сименс, {RussianType}";
        }
    }
}
