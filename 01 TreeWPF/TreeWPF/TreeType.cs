﻿namespace TreeWPF
{
    enum TreeType
    {
        Coniferous, // Хвойные
        Pine,       // Сосновые
        Cypress,    // Кипарисовые
        Fruit,      // Плодовитые
    }
}
