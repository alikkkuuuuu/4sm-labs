﻿using System;
using System.Windows;
using System.Collections.ObjectModel;
using System.Globalization;

namespace TreeWPF
{
    public partial class MainWindow : Window
    {
        ObservableCollection<Tree> trees;

        public MainWindow()
        {
            InitializeComponent();

            trees = new ObservableCollection<Tree>();
            listView_trees.ItemsSource = trees;
        }

        private void button_add_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                trees.Add(parseTree(
                    textBox_plant.Text,
                    textBox_density.Text,
                    textBox_heatConductivity.Text,
                    textBox_electricConductivity.Text,
                    textBox_treeType.Text));
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
}

        private void button_edit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                trees[trees.IndexOf(listView_trees.SelectedItem as Tree)] = parseTree(
                    textBox_plant.Text,
                    textBox_density.Text,
                    textBox_heatConductivity.Text,
                    textBox_electricConductivity.Text,
                    textBox_treeType.Text);
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button_delete_Click(object sender, RoutedEventArgs e)
        {
            trees.Remove(listView_trees.SelectedItem as Tree);
        }

        private Tree parseTree(string plantTime, string density, string heatConductivity, string electricConductivity, string rusType)
        {
            DateTime plant;
            int dens;
            int heat;
            int elec;
            TreeType type;

            if (!DateTime.TryParse(plantTime, new CultureInfo("ru-RU"), DateTimeStyles.None, out plant))
                throw new ArgumentException("Дата введена в неверном формате");
            if (!int.TryParse(density, out dens))
                throw new ArgumentException("Плотность не в форме числа");
            if (!int.TryParse(heatConductivity, out heat))
                throw new ArgumentException("Теплопроводимость не в форме числа");
            if (!int.TryParse(electricConductivity, out elec))
                throw new ArgumentException("Электропроводимость не в форме числа");
            if (!Tree.TryParseRussianType(rusType, out type))
                throw new ArgumentException($"{rusType} дерево не существует");

            return new Tree(plant, dens, heat, elec, type);
        }
    }
}
